# My Content Module

A template for creating your own Foundry VTT content module.

    "manifest": "https://gitlab.com/elpaladindx/vtt-lichdom-mmxlv/-/raw/main/module.json",
    "download": "https://gitlab.com/elpaladindx/vtt-lichdom-mmxlv/-/archive/v1.1.5/vtt-lichdom-mmxlv-v1.1.5.zip"